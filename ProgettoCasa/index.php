<?php
    session_start();
    $loginControl = include("login_control.php");
    if(!$loginControl){
        header("location: /login");
    }else{
?>
<!DOCTYPE html>
<html>
    <head>
        <link href="style.css" rel="stylesheet" type="text/css">
        <link href="fontawesome/css/all.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="navbar">Benvenuto nel servizio streaming di casa Spedicato</div>
        <div class="mainBox">
            <button onClick="window.location.href = '/film';">
                <span style="font-size: 20px;">GALLERY</span><br>
                <i class="fas fa-film"></i>
            </button>
            <div style="width: 30px;"></div>
            <form methon="get" action="logout.php">
                <button>
                    <span style="font-size: 20px;">LOGOUT</span><br>
                    <i class="fas fa-sign-out-alt"></i>
                </button>
            </form>
        </div>
    </body>
</html> 
<?php }
