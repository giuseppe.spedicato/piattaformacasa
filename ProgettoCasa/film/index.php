<!DOCTYPE html>
<html>
    <head>
        <link href="../style.css" rel="stylesheet" type="text/css">
        
    </head>
    <body>
        
        <div class="navbar film">
            FILM GALLERY 
        </div>
        <div class="table">

<?php
    function dirToArray($dir) {
        $result = array();
        $cdir = scandir($dir);
        foreach ($cdir as $key => $value)
           if (!in_array($value,array(".","..")))
              if (is_dir($dir . DIRECTORY_SEPARATOR . $value))
                 $result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value);
              else
                 $result[] = $value;
       
        return $result;
    }

    session_start();
    $loginControl = include("../login_control.php");
    if(!$loginControl)
        header("location: /login");
    else{
        $configs = include('../config.php');
        $dirElement = dirToArray($configs["filmDirectoryScan"]);
        foreach($dirElement as $element)
            if($element != 'index.php')
                echo    '<div class="listTag" >'.$element.'</div>';
    }
?>

        </div>

        <script>
            const addClickToItem = () => {
                let item = document.getElementsByClassName("listTag");
                for(let i of item)
                    i.addEventListener('click', () => window.location.href = "/film/watch?name=" + i.innerHTML, false);
            }
            addClickToItem();
        </script>

    </body>
</html>
