<?php 
session_start();
$loginControl = include("../../login_control.php");
if(!$loginControl)
    header("location: /login");
else{
    if(!array_key_exists('name', $_GET))
       header("location: /film");
    else{
        $configs = include("../../config.php");
        $url = "".$configs["filmDirectory"]."".$_GET["name"];
        ?>
        <!DOCTYPE html>
        <html>
            <head>
                <style>
                    body{margin: 0px;background: black;}
                    video{
                        width: 100vw;
                        height: 100vh;
                    }
                </style>
            </head>
            <body>
                <video controls>
                    <source src="<?php echo $url ?> " type="video/mp4"><source>
                    <source src="<?php echo $url ?> " type="video/mkv"><source>
                </video>
            </body>
        </html>
        
        <?php
    }
}
    
