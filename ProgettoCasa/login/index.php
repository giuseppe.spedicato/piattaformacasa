<?php
session_start();
$loginControl = include("../login_control.php");
if($loginControl){
    header("location: /");
}else{
?>
<!DOCTYPE html>
<html>
    <head>
        <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
        <link href="/login/login.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="loginBox">
            <div class="title">LOGIN</div>
            
            <form action="/login/make_login.php" method="post" class="form">
                <input id="USERNAME" name="USERNAME" placeholder="Username" type="text">
                <input id="PASSWORD" name="PASSWORD" placeholder="Password" type="password">
                <button type="submit">ENTRA</button>
            </form>
            <?php
                if(isset($_SESSION["error"])){
            ?>
                <div class="error">
                    <div>
                        <?php
                            echo $_SESSION['error'];
                        ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </body>
</html>
<?php }